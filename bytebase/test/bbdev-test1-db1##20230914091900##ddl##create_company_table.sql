CREATE TABLE company (
  id INT PRIMARY KEY     NOT NULL,
  name           TEXT    NOT NULL,
  address        CHAR(50) NOT NULL
);
