
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

CREATE TABLE public.company (
    id integer NOT NULL,
    name text NOT NULL,
    address character(50) NOT NULL,
    description text NOT NULL
);

CREATE TABLE public."test-first-table" (
    id integer NOT NULL,
    name text NOT NULL,
    value text NOT NULL,
    description text NOT NULL
);

COMMENT ON COLUMN public."test-first-table".id IS 'ID';

COMMENT ON COLUMN public."test-first-table".name IS 'key';

COMMENT ON COLUMN public."test-first-table".value IS 'value';

ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public."test-first-table"
    ADD CONSTRAINT "test-first-table_pkey" PRIMARY KEY (id);

